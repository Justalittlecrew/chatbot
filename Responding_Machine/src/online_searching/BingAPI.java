package online_searching;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import core.Preprocessing;
import mylibrary.MyLib_General;

public class BingAPI {

	private final String user_browser_agent = "Mozilla/5.0";
	//private static String[] stopWordList;
	
	public static String search(String inputQuery) throws Exception {

		BingAPI http = new BingAPI();

		
		return http.sendPostRequeset(inputQuery.replace(" ", "+"));


	}
	

	
	// HTTP GET request
	private String sendPostRequeset(String inputQuery) throws Exception {

		String url = "https://api.cognitive.microsoft.com/bing/v5.0/news/search?q="+inputQuery+"&count=10&offset=0&mkt=en-us&safeSearch=Moderate";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", user_browser_agent);

		con.setRequestProperty("Ocp-Apim-Subscription-Key", "6a0032b0c5694a2aa66ac067b90aca54");

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result	
		System.out.println(response.toString());

		return parseJson(response.toString());
	}

    public static String parseJson(String jsonString) throws JSONException{
    	
    	JSONObject obj = new JSONObject(jsonString);
    	String titleString="";
    	String snippetString = "";
    			
    	JSONArray arr;
    	
    	try
    	{
    		arr = obj.getJSONArray("value");
    	}
    	catch ( Exception ex){
    		return "";
    	}
    	
    	
    	for (int i = 0; i < arr.length(); i++)
    	{
    	    String title = arr.getJSONObject(i).getString("name");
    	    titleString+=" "+ title;
    	    
    	    String snippet = arr.getJSONObject(i).getString("description");
    	    snippetString+=" "+ snippet; 	    
    	 
    	
    	}    	
    	   System.out.println(titleString);
    	   
    	   System.out.println();

    	   System.out.println(snippetString);
    	   
    	 return titleString +snippetString;
    }

}