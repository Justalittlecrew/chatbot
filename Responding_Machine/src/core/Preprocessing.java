package core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import mylibrary.MyLib_General;

public class Preprocessing {
	
	
	
	private static void buildIndex() {
		System.out.print("Building index...");
		try {
			BufferedReader cmntReader = new BufferedReader(new FileReader(BotController.path_repos_cmnt));

			BufferedReader postReader = new BufferedReader(new FileReader(BotController.path_repos_post));
		
			PrintWriter writer = new PrintWriter("resources//indexFile");
			
			String line1,line2;
			
			int counter=0;
			while ((line1 = cmntReader.readLine()) != null && (line2 = postReader.readLine()) != null  ) {

				counter++;
				writer.println(MyLib_General.getFieldData(line2, 1, "\t") + "\t" + MyLib_General.getFieldData(line1, 1, "\t") );
				
				//if (counter%10000==0) System.out.println(counter);
			}
			//System.out.print(counter);
				cmntReader.close();
				postReader.close();
				writer.close();
			}catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
				
				
			}
		
		System.out.println("done!");
	}
	private static void filterPostID() {

		System.out.print("Filtering post id...");
		int counter = 0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\resources\\post\\filtered_post"));
			PrintWriter writer = new PrintWriter("resources\\post\\filtered_post_withoutid");
			String line;
			
			while ((line = reader.readLine()) != null) {
				writer.println(MyLib_General.getFieldData(line, 2, "\t") );
			}
		
			reader.close();
			writer.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println("done");
}
	
	private static void filterCommentID() {

		System.out.print("Filtering comment id...");
		int counter = 0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\resources\\cmnt\\filtered_cmnt"));
			PrintWriter writer = new PrintWriter("resources\\cmnt\\filtered_cmnt_withoutid");
			String line;
			

			while ((line = reader.readLine()) != null) {


				writer.println(MyLib_General.getFieldData(line, 2, "\t") );
				

			}
		
			reader.close();
			writer.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println("done");
}
	private static void getCommentID() {
		System.out.print("Getting comment id...");
		int counter = 0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\resources\\cmnt\\filtered_cmnt"));
			PrintWriter writer = new PrintWriter("resources\\cmnt\\filtered_cmnt_id");
			String line;
			
			while ((line = reader.readLine()) != null) {
				writer.println(MyLib_General.getFieldData(line, 1, "\t") );
			}
		
			reader.close();
			writer.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println("done");
}
	private static void getPostID() {
		System.out.print("Getting post id...");
		int counter = 0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\resources\\post\\filtered_post"));
			PrintWriter writer = new PrintWriter("resources\\post\\filtered_post_id");
			String line;
			
			while ((line = reader.readLine()) != null) {
				writer.println(MyLib_General.getFieldData(line, 1, "\t") );
			}
		
			reader.close();
			writer.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println("done");
}
	
	public static String removeStopWordFromSentence(String sentence, String[] stopWordList){
		String returnString = sentence.toLowerCase();

		for (int i=0;i<stopWordList.length;i++){			
			if (returnString.contains(" " +stopWordList[i].trim()+ " ")){				
				returnString = returnString.replace(" " + stopWordList[i].trim()+ " ", " ");
			}
		}
		return returnString.trim();
	}

	
	private  static void filterWordInRepository(int flag) {
		//flag 0 = post, 1=cmnt
		if (flag==0) 
			System.out.print("Filtering word in post...");
		else 
			System.out.print("Filtering word in comment...");		

		
		String sourcePath = "";
		String outputPath = "";
		
		if (flag ==0){
		//	sourcePath = "\\resources\\post\\repos-id-post-en";
			sourcePath=BotController.path_repos_post;
			outputPath = "resources\\post\\filtered_post";
		} else if(flag ==1){

		//	sourcePath = "\\resources\\cmnt\\repos-id-cmnt-en";
			sourcePath=BotController.path_repos_cmnt;
			outputPath = "resources\\cmnt\\filtered_cmnt";
		}else{
			System.out.print("error in filtering");
			return;
		}
		
		int stopWordCount =0;
	
		try {
			stopWordCount= 1+ MyLib_General.countLines(BotController.path_stop_word);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (stopWordCount<=10) System.out.print("problem in stop word list");
		String[] stopWordList = new String[stopWordCount];
		int tempCount=0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(BotController.path_stop_word));
			String line;
			
			while ((line = reader.readLine()) != null) {
				stopWordList[tempCount]	= line;
				tempCount+=1;
			}
			reader.close();

		} catch (Exception e) {

		}
	
		int readCount=0;

		Map<String, String> localMap = new HashMap<>();
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(sourcePath));

			String line;
			PrintWriter writer = new PrintWriter(outputPath);
			while ((line = reader.readLine()) != null) {
			//	if (readCount>=100) break;
				readCount+=1;
				
			       
				String newString =  MyLib_General.getFieldData(line, 2, "\t").trim();
				String key =  MyLib_General.getFieldData(line, 1, "\t").trim();
				if (localMap.containsKey(key))
					continue;
				else
					localMap.put(key, "");
				
			//	if (newString.contains("�")||newString.contains("�") || newString.contains("�")) continue;
				if (newString.contains("http:"))continue;
				
				newString = removePunctuation(newString);
				newString= newString.replaceAll("\\d","");
				
				newString = removeStopWordFromSentence(" "+ newString+ " ",stopWordList);
	
				int numberOfWord = newString.split(" ").length;
				if (flag==0){
					if (numberOfWord < 5 | numberOfWord > 20)
						continue;					
				}else{
					if (numberOfWord < 5 | numberOfWord > 20)
						continue;
				}
				
				if (!line.matches("\\A\\p{ASCII}*\\z")) continue;
				writer.println(MyLib_General.getFieldData(line, 1, "\t") + "\t" + newString);
						
			}
			writer.close();
			reader.close();
		//	System.out.print("postMap size:" + localMap.size());
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println("done");
	}

	public static String removePunctuation(String sentence){
		String returnString = sentence.toLowerCase();  //if all match regex should return immedinately @20170118
		
		returnString= returnString.replace("~", "");
		returnString= returnString.replace("!", "");
		returnString= returnString.replace("@", "");
		returnString= returnString.replace("$", "");
		returnString= returnString.replace("%", "");
		returnString= returnString.replace("^", "");
		returnString= returnString.replace("&", "");
		returnString= returnString.replace(",", "");
		returnString= returnString.replace(".", "");	
		returnString= returnString.replace("?", "");		
		returnString= returnString.replace("_", "");			
		returnString= returnString.replace("=", "");	
		returnString= returnString.replace("+", "");
		returnString= returnString.replace("-", "");
		returnString= returnString.replace("'s", "");	
		returnString= returnString.replace("'", "");
		returnString= returnString.replace(":", "");
		returnString= returnString.replace("(", "");
		returnString= returnString.replace(")", "");
		returnString= returnString.replace("[", "");
		returnString= returnString.replace("]", "");
		returnString= returnString.replace("|", "");
		returnString= returnString.replace("<", "");
		returnString= returnString.replace(">", "");
		returnString= returnString.replace("`", "");
		returnString= returnString.replace("#", "");
		returnString= returnString.replace("*", "");
		returnString= returnString.replace("\\", "");		
		returnString= returnString.replace("/", "");
		returnString= returnString.replace("\"", "");
		returnString= returnString.replace("{", "");		
		returnString= returnString.replace("}", "");	
		returnString= returnString.replace(";", "");	
		
		return returnString.trim();
	}	

	//public static String removeDigit(String sentence){
//		String returnString = sentence.toLowerCase();
	///	
	//	returnString = returnString.replace("\\w*\\d\\w* *", "");
		
	//	return returnString.trim();
	//}	
	
	/**
	public static void  stopWordList(){
		int counter = 0;
		
		String[] list = new String[700];	
		for (int i=0;i<list.length;i++){
			list[i] = "1";
		}
		
		try {
			BufferedReader reader = new BufferedReader(
	
					new FileReader(Application.path_stop_word));

				String line;
			

				while ((line = reader.readLine()) != null) {
					if (line.trim().equals("")) continue;
				
					list[counter]=line;
					System.out.println(list[counter]);
					counter+=1;	
				}
				
			//	System.out.println(counter);
				Arrays.sort(list);
				ArrayList temp  = new ArrayList();
				
				for (int i=0;i<list.length;i++){
					temp.add(list[i]);
				}
				
				for (int i=temp.size()-1;i>=0;i--){
					if (temp.indexOf(temp.get(i)) !=i)
						temp.remove(i);
				}		
				
				
				
				PrintWriter writer = new PrintWriter("stop_word_list");
			
				
					for (int i=0;i<temp.size();i++){
						writer.println(temp.get(i));

						
					}			

			
				writer.close();
				

				System.out.print("done" + counter);
			reader.close();
		
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}

		
		
		
	}
	**/
	private static void  calculateCommentTermFrequency(){

		System.out.print("Calculating comment term frequency...");
	    Map<String, Integer> map = new HashMap<>();		
		
		int counter = 0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\resources\\cmnt\\filtered_cmnt_withoutid"));
				
			String line;
			
			while ((line = reader.readLine()) != null) {
				
			    
				 	String filteredDuplicated = MyLib_General.deDup(line);
				
				    for (String w : filteredDuplicated.split(" ")) {   //only get the document numbe that contain word
				 
				    	if (w.trim().equals("")) continue;
				    	Integer n = map.get(w);
				        n = (n == null) ? 1 : ++n;
				        map.put(w, n);
				    }
				    counter+=1;
				   // if (counter % 1000 ==0) System.out.println(counter);
			}
		
			reader.close();
		
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
	    String [] termList = new String [map.size()];
	   
	    int tempCounter =0;
	    for (Object key : map.keySet()) {
	    	termList[tempCounter] = key + "\t" + map.get(key);
            tempCounter+=1;
        }
	    Arrays.sort(termList);
		PrintWriter writer;
		try {
			writer = new PrintWriter("resources\\cmnt\\term_frequency");
			
			   for (String term : termList) {
				   writer.println(term);
		        }
			   	writer.close();
	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("done");
		
	}
	private  static void  calculatePostTermFrequency(){
		System.out.print("Calculating post term frequency...");
		
	    Map<String, Integer> map = new HashMap<>();		
		int counter = 0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\resources\\post\\filtered_post_withoutid"));
				
			String line;
			
			while ((line = reader.readLine()) != null) {
				 	String filteredDuplicated = MyLib_General.deDup(line);
				    for (String w : filteredDuplicated.split(" ")) {   //only get the document numbe that contain word
				 
				    	if (w.trim().equals("")) continue;
				    	Integer n = map.get(w);
				        n = (n == null) ? 1 : ++n;
				        map.put(w, n);
				    }
				    counter+=1;
			//	    if (counter % 1000 ==0) System.out.println(counter);
			}
		
			reader.close();
		
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
	    String [] termList = new String [map.size()];
	   
	    int tempCounter =0;
	    for (Object key : map.keySet()) {
	    	termList[tempCounter] = key + "\t" + map.get(key);
            tempCounter+=1;
        }
	    Arrays.sort(termList);
		PrintWriter writer;
		try {
			writer = new PrintWriter("resources//post//term_frequency");
			
			   for (String term : termList) {
				   writer.println(term);
		        }
			   	writer.close();
	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("done!" );
		
	}
	public static void processingStepTwo(){
		Embedding.buildPostCmntCosineSimilarityScore();
		Embedding.buildPostCmntSentencesEmbeddingScore();
		
		Embedding.createSentencesEmbedding(0);
		Embedding.createSentencesEmbedding(1);
		
		System.out.println("done post and cmnt");
	}
	public static void processingStepOne(){
		
		Preprocessing.buildIndex();    // build the index between post and index, one time use only
		
		
		// output two file, namely filtered_cmnt_id and filtered_cmnt_withoutid
		filterWordInRepository(0);
		filterWordInRepository(1);		
		System.out.println("done filterword");
		
		getCommentID();
		getPostID();
		System.out.println("done getcommentID");
		
		
		filterCommentID();
		filterPostID();
		
		//calculated word frequency
		calculateCommentTermFrequency();
		calculatePostTermFrequency();
		System.out.println("done calculateTermFrequency");
	
	
		
	}

}
