package mylibrary;

import java.util.ArrayList;

public class MyLib_NLP {
	
	public static double cosine_Similarity(String Text1,double[] QueryWeighting, String Text2, double[] commentWeighting) {
	
		ArrayList VectorString = getVectorString(Text1, Text2);
		
		int[] v1 = new int[VectorString.size()];
		int[] v2 = new int[VectorString.size()];
	
		double[] weight = new double[v2.length];
	
		for (int i = 0; i < v1.length; i++) {
			v1[i] = 0;
			v2[i] = 0;
			weight[i] = 0;
		}
	
	
		v1 = getVector(Text1, Text2, 1);
		v2 = getVector(Text1, Text2, 2);
		
		
		String[] temp1 = MyLib_General.deDup(Text1).split(" ");		
		String[] temp = MyLib_General.deDup(Text2).split(" ");
		
		for (int j = 0; j < QueryWeighting.length; j++) {	
			int insertIndex = VectorString.indexOf(temp1[j]);
			weight[insertIndex] = QueryWeighting[j]; 	
			
		}	
	
	
		
		for (int j = 0; j < commentWeighting.length; j++) {	
			int insertIndex = VectorString.indexOf(temp[j]);
			if (weight[insertIndex]==0)
			weight[insertIndex] = commentWeighting[j]; 	
		
		}	
		
	
		double dotProduct = 0.0;
		double normA = 0.0;
		double normB = 0.0;
		for (int i = 0; i < v1.length; i++) {
			dotProduct += v1[i] * v2[i] * weight[i];
			normA += Math.pow(v1[i], 2) * weight[i];
			normB += Math.pow(v2[i], 2) * weight[i];
	
		}
	
		return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	
	}
	public static int[] getVector(String Text1, String Text2, int targetVector	){
		

		String[] word_seq_text1 = Text1.split(" ");
		String[] word_seq_text2 = Text2.split(" ");

		ArrayList vectorString = new ArrayList();

		
		for (int i = 0; i < word_seq_text1.length; i++) {
			if (!vectorString.contains(word_seq_text1[i])) 
				vectorString.add(word_seq_text1[i]);
		}		
		for (int i = 0; i < word_seq_text2.length; i++) {
			if (!vectorString.contains(word_seq_text2[i])) 
				vectorString.add(word_seq_text2[i]);
		}			
		
		if (targetVector ==1){
			int[] v1 = new int[vectorString.size()];
			for (int i = 0; i < word_seq_text1.length; i++) 
				v1[vectorString.indexOf(word_seq_text1[i])] = 1;
			return v1;
		}
		else{
			int[] v2 = new int[vectorString.size()];
			for (int i = 0; i < word_seq_text2.length; i++) 
				v2[vectorString.indexOf(word_seq_text2[i])] = 1;
			return v2;
		}

	}
	public static ArrayList getVectorString(String Text1, String Text2	){
		

		String[] word_seq_text1 = Text1.split(" ");
		String[] word_seq_text2 = Text2.split(" ");

		ArrayList vectorString = new ArrayList();

		
		for (int i = 0; i < word_seq_text1.length; i++) {
			if (!vectorString.contains(word_seq_text1[i])) 
				vectorString.add(word_seq_text1[i]);
		}		
		for (int i = 0; i < word_seq_text2.length; i++) {
			if (!vectorString.contains(word_seq_text2[i])) 
				vectorString.add(word_seq_text2[i]);
		}			
		
		return vectorString;
	}
}
