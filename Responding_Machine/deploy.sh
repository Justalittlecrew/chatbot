if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 1
fi

unzip -o  resources.zip
#echo $1
repo_dir=$1
mkdir -p $1/Responding_Machine/resources/cmnt/
mkdir -p $1/Responding_Machine/resources/Embedding/

mv filteredCommentWithWeighting $1/Responding_Machine/resources/cmnt/
mv filteredCommentWithWeighting_0 $1/Responding_Machine/resources/cmnt/
mv filteredCommentWithWeighting_1 $1/Responding_Machine/resources/cmnt/
mv filteredCommentWithWeighting_2 $1/Responding_Machine/resources/cmnt/
mv term_frequency $1/Responding_Machine/resources/cmnt/
mv cmnt_smaller_word_embedding $1/Responding_Machine/resources/Embedding/
mv repos-id-cmnt-en $1/Responding_Machine/resources/cmnt/
